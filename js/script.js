$(function() {

	$('body').on('click','.delete',(function(e) {
		var id = $(this).data('id');
		$("#image_id").val(id);
	}));

	$('body').on('click','.edit',(function(e) {
		var id = $(this).data('id');
		var image = $(this).data('image');
		var title = $(this).data('title');

		$("#edit_image_id").val(id);
		$('#edit_previewing').attr('src',image);
		$("#edit_title").val(title);

	}));

	$("#add_image_form").on('submit',(function(e) {
		e.preventDefault();
		$("#message").empty();
		$.ajax({
			url: $('#add_image_form').attr("action"), // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData( this ), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				$('#myModal').modal('toggle');
				$(".table-header").after(data);
				$("#image").wrap('<form>').closest('form').get(0).reset();
    			$("#image").unwrap();
				$("#title").wrap('<form>').closest('form').get(0).reset();
    			$("#title").unwrap();
    			$('#previewing').attr('src','https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview');

			}
		});
		return false;
	}));

	$("#edit_image_form").on('submit',(function(e) {
		e.preventDefault();
		var id = $("#edit_image_id").val();
		$("#message").empty();
		$.ajax({
			url: $('#edit_image_form').attr("action"), // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: new FormData( this ), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			contentType: false,       // The content type used when sending data to the server.
			cache: false,             // To unable request pages to be cached
			processData:false,        // To send DOMDocument or non processed data file it is set to false
			success: function(data)   // A function to be called if request succeeds
			{
				$(".image_"+id).hide('slow');
				$('#myModalEdit').modal('toggle');
				$(".table-header").after(data);
				$("#edit_image").wrap('<form>').closest('form').get(0).reset();
    			$("#edit_image").unwrap();
				$("#edit_title").wrap('<form>').closest('form').get(0).reset();
    			$("#edit_title").unwrap();
    			$('#previewing').attr('src','https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview');

			}
		});
		return false;
	}));

	$("#delete_image").on('click',(function(e) {
		e.preventDefault();
		$.ajax({
			url: $('#delete_image_form').attr("action"), // Url to which the request is send
			type: "POST",             // Type of request to be send, called as method
			data: $('#delete_image_form').serializeArray(), // Data sent to server, a set of key/value pairs (i.e. form fields and values)
			success: function(data)   // A function to be called if request succeeds
			{
				$('#myModalDelete').modal('toggle');
				$(".image_"+$("#image_id").val()).hide('slow');
			}
		});
	}));

	$("#image").change(function() {
		$("#message").empty(); // To remove the previous error message
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			$('#previewing').attr('src','https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview');
			$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});

	$("#edit_image").change(function() {
		$("#message").empty(); // To remove the previous error message
		var file = this.files[0];
		var imagefile = file.type;
		var match= ["image/jpeg","image/png","image/jpg"];
		if(!((imagefile==match[0]) || (imagefile==match[1]) || (imagefile==match[2])))
		{
			$('#edit_previewing').attr('src','https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview');
			$("#message").html("<p id='error'>Please Select A valid Image File</p>"+"<h4>Note</h4>"+"<span id='error_message'>Only jpeg, jpg and png Images type allowed</span>");
			return false;
		}
		else
		{
			var reader = new FileReader();
			reader.onload = imageIsLoaded;
			reader.readAsDataURL(this.files[0]);
		}
	});

	function imageIsLoaded(e) {
		$("#file").css("color","green");
		$('#image_preview').css("display", "block");
		$('#previewing').attr('src', e.target.result);
		$('#previewing').addClass('img-responsive');
	};
});