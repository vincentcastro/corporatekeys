<div class="row">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
		  Add
		</button>
		<hr>
		<table class="table table-hover">
		  <tr class="table-header">
		  	<th> Title </th>
		  	<th> Thumbnail </th>
		  	<th> Filename </th>
		  	<th> Date </th>
		  	<th> Action </th>
		  </tr>
		  <?php foreach ($images as $image) { ?>
		  <tr class="image_<?=$image->image_id?>">
		  	<td> <?=$image->image_title?> </td>
		  	<td> <img src="<?=$image->image_path?>" class="img-responsive"/> </td>
		  	<td> <?=$image->image_filename?> </td>
		  	<!-- td> <?=date('F d, Y',strtotime($image->image_date))?> </td -->
		  	<td> <?=date('Y-m-d',strtotime($image->image_date))?> </td>
		  	<td> 
		  		<a href="#" data-id="<?=$image->image_id?>" data-title="<?=$image->image_title?>" data-image="<?=$image->image_path?>" class="edit" data-toggle="modal" data-target="#myModalEdit"> Edit </a>| 
		  		<a href="#" data-id="<?=$image->image_id?>" class="delete" data-toggle="modal" data-target="#myModalDelete"> Delete </a> 
		  	</td>
		  </tr>
		  <?php } ?>
		</table>
	</div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" id="add_image_form" method="POST" action="<?php echo URL::site('images/add') ?>" enctype="multipart/form-data">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Add Picture</h4>
	      </div>
	      <div class="modal-body">
			      <input type="hidden" class="form-control" name="filename" id="filename" >
			      <input type="hidden" class="form-control" name="path" id="path" >
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
			    <div class="col-sm-10">
			      <div id="image_preview" class="col-sm-12 text-center"><img id="previewing" src="https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview" /></div>
				  <div id="message"></div>
			      <input type="file" class="form-control" name="image" id="image" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Title</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="title" id="title" >
			    </div>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <input type="submit" class="btn btn-primary" id="add_image" value="Add" />
	      </div>
	  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalEdit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" id="edit_image_form" method="POST" action="<?php echo URL::site('images/add') ?>">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Edit Picture</h4>
	      </div>
	      <div class="modal-body">
			  <input type="hidden" class="form-control" name="image_id" id="edit_image_id">
			  <div class="form-group">
			    <label for="inputEmail3" class="col-sm-2 control-label">Image</label>
			    <div class="col-sm-10">
			      <div id="image_preview" class="col-sm-12 text-center"><img id="edit_previewing" src="https://dummyimage.com/200x200/ccc/ffffff.png&text=Preview" /></div>
				  <div id="message"></div>
			      <input type="file" class="form-control" name="image" id="edit_image" >
			    </div>
			  </div>
			  <div class="form-group">
			    <label for="inputPassword3" class="col-sm-2 control-label">Title</label>
			    <div class="col-sm-10">
			      <input type="text" class="form-control" name="title" id="edit_title" >
			    </div>
			  </div>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <input type="submit" class="btn btn-primary" id="edit_image" value="Edit" />
	      </div>
	  </form>
    </div>
  </div>
</div>

<div class="modal fade" id="myModalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" id="delete_image_form" method="POST" action="<?php echo URL::site('images/delete') ?>">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Delete Picture</h4>
	      </div>
	      <div class="modal-body">
	      	You really want to delete this?
			      <input type="hidden" class="form-control" name="image_id" id="image_id">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-danger" id="delete_image" > Delete </button>
	      </div>
	  </form>
    </div>
  </div>
</div>