<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller {

	// In your controller:
	 
	public function action_index()
	{
	    $view = View::factory('template/template');
	 
	    $view->title = "CorporateKeys Application"; 
	    $images = ORM::factory('Images')->order_by('image_id','desc')->find_all();
	    $view->body = View::factory('index')->bind('images', $images);

	    // Render the view
	    $home = $view->render();
	 
	    $this->response->body($home);
	}

} // End Welcome
