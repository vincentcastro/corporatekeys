<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Images extends Controller {

	// In your controller:
	 
	public function action_add()
	{

                if ($this->request->method() == Request::POST)
                {

                        try
                        {
                                date_default_timezone_set('Asia/Taipei');
                                $date = date('Y-m-d');
                                $title = $_POST['title'];

                                if(isset($_POST['image_id'])){
                                        $image = ORM::factory('Images', $_POST['image_id']);
                                } else {
                                        $image = ORM::factory('Images');
                                }

                                // Do an INSERT query
                                $image->image_title = $title;
                                if( !$_FILES['image']['error'] ){
                                        $filename = $this->_save_image($_FILES['image']);
                                        $path = 'uploads/'.$filename;
                                        $image->image_path = $path;
                                        $image->image_filename = $_FILES['image']['name'];
                                }
                                $image->image_date = $date;
                                $image->save();
                                
                                $view = ORM::factory('Images', $image);

                                echo $this->render_tr($view);

                        }
                        catch (ORM_Validation_Exception $e)
                        {
                            var_dump($e);
                            die();
                        }
                	// Create an instance of a model
                        

                }

	}

        public function render_tr($view)
        {
                $return = "";
                $return .= "<tr class='image_".$view->image_id."'>";
                $return .= "<td>".$view->image_title."</td>";
                $return .= "<td> <img src='".$view->image_path."' class='img-responsive' > </td>";
                $return .= "<td>".$view->image_filename."</td>";
                $return .= "<td>".$view->image_date."</td>";
                $return .= "<td>";
                $return .= "<a href='#' data-id='".$view->image_id."' data-title='".$view->image_title."' data-image='".$view->image_path."' class='edit' data-toggle='modal' data-target='#myModalEdit'> Edit </a>|";
                $return .= "<a href='#' data-id='".$view->image_id."' class='delete' data-toggle='modal' data-target='#myModalDelete'> Delete </a>";
                $return .= "</td>";
                $return .= "</tr>";

                return $return;
        }

        public function action_delete()
        {

                if ($this->request->method() == Request::POST)
                {

                        $image = DB::delete('Images')
                                ->where('image_id', '=', $_POST['image_id'])
                                ->execute(Database::instance());;
                        //$image->delete();

                }

        }

        public function action_upload()
        {
                $view = View::factory('avatar/upload');
                $error_message = NULL;
                $filename = NULL;

                if ($this->request->method() == Request::POST)
                {
                    if (isset($_FILES['avatar']))
                    {
                        $filename = $this->_save_image($_FILES['avatar']);
                    }
                }

                if ( ! $filename)
                {
                    $error_message = 'There was a problem while uploading the image.
                        Make sure it is uploaded and must be JPG/PNG/GIF file.';
                }

                $view->uploaded_file = $filename;
                $view->error_message = $error_message;
                $this->response->body($view);
        }

        protected function _save_image($image)
        {
                if (
                    ! Upload::valid($image) OR
                    ! Upload::not_empty($image) OR
                    ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
                {
                    return FALSE;
                }

                $directory = DOCROOT.'uploads/';

                if ($file = Upload::save($image, NULL, $directory))
                {
                    $filename = strtolower(Text::random('alnum', 20)).'.jpg';

                    Image::factory($file)
                        ->resize(200, 200, Image::AUTO)
                        ->save($directory.$filename);

                    // Delete the temporary file
                    unlink($file);

                    return $filename;
                }

                return FALSE;
        }

} // End Welcome
