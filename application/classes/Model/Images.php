<?php defined('SYSPATH') or die('No direct script access.');

class Model_Images extends ORM
{
	protected $_db_group = 'default';
    protected $_table_name = 'images';
	protected $_primary_key = 'image_id';
}